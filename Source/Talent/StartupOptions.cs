﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Talent.Content;
using Talent.ControlFlow;

namespace Talent
{
    public static class Startup
    {
        public class Options
        {
            public readonly Boolean UseDefaultContentParsers;
            public readonly IReadOnlyDictionary<Type, ContentParserDelegate> CustomContentParsers;
            public readonly InitialStateDelegate InitialStateDelegate;

            public Options(InitialStateDelegate initialStateDelegate,
                           Boolean useDefaultContentParsers, 
                           IDictionary<Type, ContentParserDelegate> customContentParsers)
            {
                UseDefaultContentParsers = useDefaultContentParsers;
                CustomContentParsers = new Dictionary<Type, ContentParserDelegate>(customContentParsers);
                InitialStateDelegate = initialStateDelegate;
            }
        }

        public static void Run(Options options)
        {
            using (Runner r = new Runner(options))
            {
                r.Run();
            }
        }

        public delegate State InitialStateDelegate();
    }

}