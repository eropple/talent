﻿using System;
using System.IO;
using System.Reflection;
using Talent.Exceptions;

namespace Talent
{
    public static class Platform
    {
#if DESKTOP
        public static readonly String Company;
        public static readonly String Product;
        public static readonly String ExecutableRoot;
        public static readonly String CoreResourcesRoot;
        public static readonly String UserRoot;

        static Platform()
        {
            try
            {
                Assembly a = Assembly.GetEntryAssembly();

                Company =
                    ((AssemblyCompanyAttribute)
                     Attribute.GetCustomAttribute(a, typeof(AssemblyCompanyAttribute), false)).Company;
                Product =
                    ((AssemblyProductAttribute)
                     Attribute.GetCustomAttribute(a, typeof(AssemblyProductAttribute), false)).Product;

                ExecutableRoot = Path.GetDirectoryName(a.Location);

                CoreResourcesRoot = Path.Combine(ExecutableRoot, "Resources");

                UserRoot = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                        Company, Product);
                if (!Directory.Exists(UserRoot)) Directory.CreateDirectory(UserRoot);
            }
            catch (Exception ex)
            {
                throw new PlatformException("Your entry assembly must have Company and Product attributes.", ex);
            }
        }
    }
#endif
}
