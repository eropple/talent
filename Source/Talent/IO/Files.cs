﻿using SharpFileSystem;
using SharpFileSystem.FileSystems;
using Talent;

namespace Anaeax.IO
{
    public static class Files
    {
        /// <summary>
        /// The read-only file system in which the game executable resides.
        /// </summary>
        public static readonly ReadOnlyFileSystem Executable;
        /// <summary>
        /// The read-only file system in which game data is stored as a hierarchy.
        /// </summary>
        public static readonly ReadOnlyFileSystem GameData;

        /// <summary>
        /// The WRITABLE file system in which saves are written.
        /// </summary>
        public static readonly IFileSystem SaveGames;

        static Files()
        {
#if DESKTOP
            PhysicalFileSystem executable = new PhysicalFileSystem(Platform.ExecutableRoot);
            Executable = new ReadOnlyFileSystem(executable);


            PhysicalFileSystem u = new PhysicalFileSystem(Platform.UserRoot);

            FileSystemPath sg_path = FileSystemPath.Root.AppendDirectory("Saves");
            if (!u.Exists(sg_path)) u.CreateDirectory(sg_path);
            SubFileSystem sg = new SubFileSystem(u, sg_path);
            SaveGames = sg;

#if WINDOWS
            var dataPath = FileSystemPath.Root.AppendDirectory("Data");
            SubFileSystem coreData = new SubFileSystem(executable, dataPath);
            SubFileSystem userData = new SubFileSystem(u, dataPath);
#else
#error TODO: provide a path for getting coreData and userData
#endif
            Mods.Initialize(userData, coreData);
            GameData = Mods.GameData;
#else
#error TODO: implement initializer for Files
#endif


#if DEBUG
            foreach (FileSystemPath p in GameData.GetEntitiesRecursive(FileSystemPath.Root))
            {
                Log.Debug(p.ToString());
            }
#endif
        }



    }
}
