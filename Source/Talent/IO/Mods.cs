﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharpFileSystem;
using SharpFileSystem.FileSystems;
using Talent;
using Talent.IO;

namespace Anaeax.IO
{
    public static class Mods
    {
        public static ReadOnlyFileSystem GameData { get; private set; }
        public static IEnumerable<Mod> ActiveMods { get; private set; }

        private static Boolean _initialized = false;
        public static void Initialize(params IFileSystem[] fileSystems)
        {
            if (_initialized) throw new InvalidOperationException("Can only init Mods once.");
            _initialized = true;

            GameData = BuildGameDataHierarchy(fileSystems);
        }




        public static IEnumerable<FileSystemPath> GetDirectoriesMatchingPath(String path)
        {
            if (!path.EndsWith("/")) path += "/";
            return ActiveMods.Select(m => FileSystemPath.Root.AppendPath(path))
                             .Where(p => GameData.Exists(p));
        }


        private static ReadOnlyFileSystem BuildGameDataHierarchy(IFileSystem[] fileSystems)
        {
            // note: file systems are in descending priority order.

            List<String> mods;
            Dictionary<String, IFileSystem> modFSMap;
            GetEnabledValidatedMods(fileSystems, out mods, out modFSMap);

            // at this point we know that anything in modNames exists, as a mod, in
            // at least one of our file systems. So we now walk for any overrides that need
            // to be inserted, then the mod itself.

            // We also know that anything in modName exists in modFSMap, so we don't
            // have to re-iterate.

            Dictionary<String, List<IFileSystem>> mountLists = new Dictionary<String, List<IFileSystem>>(mods.Count);
            foreach (String mod in mods) mountLists.Add(mod, new List<IFileSystem>());

            FileSystemPath overridePath = FileSystemPath.Root.AppendDirectory("Overrides");

            // Once a mod has been parsed, nobody else can override it.
            HashSet<String> alreadyParsedMods = new HashSet<String>();

            foreach (String mod in mods)
            {
                if (alreadyParsedMods.Contains(mod)) continue;

                IFileSystem realFS = modFSMap[mod];

                SubFileSystem modFS = new SubFileSystem(realFS, FileSystemPath.Root.AppendDirectory(mod));

                mountLists[mod].Add(modFS); // add own file system to own list
                alreadyParsedMods.Add(mod);

                if (!modFS.Exists(overridePath)) continue;

                foreach (FileSystemPath modOverride in modFS.GetEntities(overridePath))
                {
                    // if it's not a directory or if the name isn't in mountLists (not an enabled mod)
                    if (!modOverride.IsDirectory || !mountLists.ContainsKey(modOverride.EntityName) ||
                        alreadyParsedMods.Contains(modOverride.EntityName)) continue;

                    // self-overrides would be really stupid.
                    if (mod == modOverride.EntityName)
                    {
                        Log.Error("Error parsing mod '{0}': attempts to override itself. Ignoring self-override.");
                        continue;
                    }

                    mountLists[modOverride.EntityName].Add(new SubFileSystem(modFS, modOverride));
                }
            }

            // mount each assembled file list at its own mount point...

            FileSystemMounter fs = new FileSystemMounter();
            fs.Mounts.Add(new KeyValuePair<FileSystemPath, IFileSystem>(FileSystemPath.Root, new MergedFileSystem()));
            foreach (KeyValuePair<String, List<IFileSystem>> kvp in mountLists)
            {
                // saves us a recursive call; the whole thing gets wrapped in
                // a ReadOnlyFileSystem so the exact semantics don't bother me.
                IFileSystem newFS = kvp.Value.Count == 1 ? kvp.Value[0] : new MergedFileSystem(kvp.Value);

                fs.Mounts.Add(new KeyValuePair<FileSystemPath, IFileSystem>(
                                      FileSystemPath.Root.AppendDirectory(kvp.Key),
                                      newFS
                                  ));

            }

            return new ReadOnlyFileSystem(fs);
        }

        private static void GetEnabledValidatedMods(IFileSystem[] fileSystems,
            out List<String> modNames, out Dictionary<String, IFileSystem> modFSMap)
        {
            modNames = new List<String>(Configuration.Get("enabled.mods", "")
                            .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
            modNames.Add("Core"); // core always comes last, you can't disable it.

            modFSMap = new Dictionary<String, IFileSystem>(modNames.Count);

            HashSet<String> killList = new HashSet<String>();

            List<Mod> mods = new List<Mod>(modNames.Count);

            foreach (String modName in modNames)
            {
                FileSystemPath mod = FileSystemPath.Root.AppendDirectory(modName);
                FileSystemPath modInfo = mod.AppendFile("info.xml");

                IFileSystem fs = null;
                foreach (IFileSystem fileSystem in fileSystems)
                {
                    if (fileSystem.Exists(mod) && fileSystem.Exists(modInfo))
                    {
                        fs = fileSystem;
                        break;
                    }
                }

                if (fs != null)
                {
                    try
                    {
                        Mod modFile = Mod.FromXml(fs.OpenFile(modInfo, FileAccess.Read), modName);
                        modFSMap.Add(modName, fs);
                        mods.Add(modFile);
                        Log.Info("Validated module: {0}", modName);
                    }
                    catch (InvalidDataException ex)
                    {
                        Log.Error("Mod {0} validated, but info.xml failed to parse.", modName);
                        Log.Error(ex.InnerException.Message);
                        Log.Error(ex.InnerException.StackTrace);
                    }
                }
                else
                {
                    Log.Info("Could not validate module: {0}", modName);
                    killList.Add(modName);
                }
            }

            modNames.RemoveAll(x => killList.Contains(x));

            ActiveMods = mods;
        }
    }
}
