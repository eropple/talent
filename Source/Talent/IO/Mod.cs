﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Talent.Utils;

namespace Talent.IO
{
    public class Mod
    {
        /// <summary>
        /// The internal name of the mod (the same as the mod's directory name).
        /// </summary>
        public readonly String Name;

        /// <summary>
        /// The title that will be used in the mod selector and,
        /// if this is a BaseMod, the "choose your game" screen.
        /// TODO: Write the "choose your game" screen.
        /// </summary>
        public readonly String Title;

        /// <summary>
        /// A short, multi-line description that will be used in the mod selector and,
        /// if this is a BaseMod, the "choose your game" screen.
        /// </summary>
        public readonly IEnumerable<String> Description;

        /// <summary>
        /// If true, this mod can be used from the aforementioned "choose your mod" screen,
        /// where its init/intro scripts will be used (instead of the default core mod).
        /// </summary>
        public readonly Boolean BaseMod;

        /// <summary>
        /// Fluff text to provide the author's name.
        /// </summary>
        public readonly String Author;

        /// <summary>
        /// Fluff text to provide a meaningless "version" number.
        /// </summary>
        public readonly String Version;

        private Mod(String name, String title, IEnumerable<String> description,
                    String author, String version, Boolean baseMod)
        {
            Name = name;
            Title = title;
            Description = description;
            Author = author;
            Version = version;
            BaseMod = baseMod;
        }

        public static Mod FromXml(Stream stream, String modName = null)
        {
            try
            {
                XDocument doc = XDocument.Load(stream);

                XElement root = doc.Element("mod");

                String name = root.Attribute("name").Value;
                if (modName != null && name != modName)
                {
                    throw new InvalidDataException(String.Format("modName = '{0}', info modName = '{1}'", modName, name));
                }

                String title = root.Attribute("title").Value;
                IEnumerable<String> description = (root.Element("description").Value)
                                                    .Split('\n')
                                                    .Select(line => line.Trim());
                String author = root.Attribute("author").Value;
                String version = root.Attribute("version").Value;

                Boolean baseMod = Booleans.ExtendedParse(root.Attribute("base").Value);

                return new Mod(name, title, description, author, version, baseMod);
            }
            catch (Exception ex)
            {
                if (modName != null)
                {
                    throw new InvalidDataException("Failed to parse info.xml for mod '" + modName + "'.", ex);
                }

                throw new InvalidDataException("Failed to parse info.xml for anonymous mod.", ex);
            }
        }
    }
}
