﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Talent.Content;
using Talent.Input;

namespace Talent.ControlFlow
{
    public abstract class State : IInputHandler, IDisposable
    {
        private Boolean _initialized;

//        protected readonly ContentContext GlobalContent;
//        protected readonly ContentContext LocalContent;

        protected ContentContext GlobalContent { get; private set; }
        protected ContentContext LocalContent { get; private set; }
        protected StateSystem StateSystem { get; private set; }
        protected GraphicsDevice Device { get; private set; }

        protected State()
        {
        }

        public abstract Boolean ShouldSoakDraws { get; }
        public abstract Boolean ShouldSoakInput { get; }
        public abstract Boolean ShouldSoakUpdates { get; }


        

        public Boolean IsInitialized { get { return _initialized; } }

        public abstract void Initialize();
        public void DoInitialize(StateSystem stateSystem)
        {
            if (_initialized == false)
            {
                _initialized = true;

                StateSystem = stateSystem;
                Device = StateSystem.Graphics.GraphicsDevice;
                GlobalContent = stateSystem.GlobalContent;
                LocalContent = new ContentContext(GlobalContent);

                Initialize();
            }
        }

        public abstract void Update(Double delta, Boolean topOfStack);
        public void DoUpdate(Double delta, Boolean topOfStack)
        {
            Update(delta, topOfStack);
        }
        public abstract void Draw(Double delta, SpriteBatch batch, Boolean topOfStack);
        public void DoDraw(Double delta, SpriteBatch batch, Boolean topOfStack)
        {
            Draw(delta, batch, topOfStack);
        }


        public void Dispose()
        {
            DoDispose();
        }
        public virtual void DoDispose() { }


        public virtual void LostFocus(State newTopState)
        {
        }
        public virtual void GotFocus(State oldTopState)
        {
        }

        public virtual Boolean KeyDown(Keys k, Boolean control, Boolean shift, Boolean alt) { return true; }
        public virtual Boolean KeyUp(Keys k) { return true; }

        public virtual Boolean MouseButtonPressed(MouseButton button, Vector2 position, Boolean control, Boolean shift, Boolean alt) { return true; }
        public virtual Boolean MouseButtonReleased(MouseButton button, Vector2 position) { return true; }
        public virtual Boolean MouseScrollWheel(Int32 scrollDirection, Vector2 position) { return true; }
        public virtual Boolean MouseMoved(Vector2 position, Vector2 delta) { return true; }

        public virtual Boolean TouchPressed(Int32 id, Vector2 position) { return true; }
        public virtual Boolean TouchMoved(Int32 id, Vector2 position, Vector2 delta) { return true; }
        public virtual Boolean TouchReleased(Int32 id, Vector2 position) { return true; }

        public virtual Boolean GamePadThumbstickMoved(PlayerIndex index, Thumbstick stick, Vector2 position, Vector2 delta) { return true; }
        public virtual Boolean GamePadTriggerMoved(PlayerIndex index, Trigger trigger, Single position, Single delta) { return true; }
        public virtual Boolean GamePadEventDown(PlayerIndex index, GamePadEvent eventType) { return true; }
        public virtual Boolean GamePadEventUp(PlayerIndex index, GamePadEvent eventType) { return true; }
        
        public virtual Boolean GamePadAttached(PlayerIndex index) { return true; }
        public virtual Boolean GamePadDetached(PlayerIndex index) { return true; }
    }
}