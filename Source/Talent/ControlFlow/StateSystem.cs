﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Talent.Content;
using Talent.Graphics.Fonts;
using Talent.Graphics.Sprites.Text;
using Talent.Input;

namespace Talent.ControlFlow
{
    public sealed class StateSystem : IInputHandler
    {
        private readonly List<State> _states = new List<State>(50); // should never hit that capacity!

        private Boolean _showFPS;
        private Boolean _skipDraw;
        private SpriteBatch _batch;
        private TextBuilder _debugFont;

        public readonly GraphicsDeviceManager Graphics;
        public readonly ContentContext GlobalContent;

        public StateSystem(GraphicsDeviceManager graphics, ContentContext rootContext)
        {
            Graphics = graphics;
            GlobalContent = rootContext;
            _batch = new SpriteBatch(Graphics.GraphicsDevice);
            
            _showFPS = Configuration.Get("show.fps", Debugger.IsAttached);
            _debugFont = GlobalContent.Load<TextBuilder>("Core/Fonts/debug_font.fnt");

#if DEBUG
            Runner.Instance.IsMouseVisible = true;
#else
            Runner.Instance.IsMouseVisible = Configuration.Get("mouse.hardware", false);
#endif
        }

        internal void BeforeStart()
        {
            _batch = new SpriteBatch(Graphics.GraphicsDevice);
        }

        internal void Update(Double delta)
        {
            _skipDraw = false;

            Int32 count = _states.Count;
            Int32 last = count - 1;

            State topState = _states[last];

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                s.DoUpdate(delta, i == last);
                if (s.ShouldSoakUpdates) break;
            }
        }

        internal void Draw(Double delta)
        {
            if (_skipDraw) return;

            Int32 count = _states.Count;
            Int32 last = count - 1;

            Int32 drawSoak = 0;

            if (count < 1) return;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.ShouldSoakDraws)
                {
                    drawSoak = i;
                    break;
                }
            }

            if (drawSoak < 0) drawSoak = 0;

            Rectangle r = _batch.GraphicsDevice.ScissorRectangle;
            for (Int32 i = drawSoak; i < count; ++i)
            {
                State s = _states[i];
                if (s == null) continue;

                _batch.BeginDefaults();
                s.DoDraw(delta, _batch, i == last);
                _batch.End();

                _batch.GraphicsDevice.ScissorRectangle = r;
            }

            //            if (Core.Settings.ShowOverlay)
            //            {
            //                BitmapFont font = Core.GetOverlayFont();
            //
            //                _batch.BeginScaled();
            //                String text = String.Format("# Screens: {0}   Top: {1}", _states.Count, _states[_states.Count - 1]);
            //                font.DrawString(_batch, text, 5, 0 + font.MeasureStringHeight(), Color.White);
            //                _batch.End();
            //            }

            if (_showFPS)
            {
                _batch.BeginDefaults();
#if DEBUG
                String s = String.Format("FPS: {0:0}\nTop: {1}", (1.0/delta), _states.Last().GetType());
#else
                String s = String.Format("FPS: {0:0}", (1.0/delta));
#endif
                TextSprite t = _debugFont.BuildTextSprite(s);
                t.Draw(delta, _batch, new Vector2(0, _batch.GraphicsDevice.Viewport.Height - t.Height));

                _batch.End();
            }

            if (!Runner.Instance.IsMouseVisible) RenderSoftwareMouse();
        }


        public void SkipDrawThisFrame()
        {
            _skipDraw = true;
        }

        public Int32 Count { get { return _states.Count; } }



        public void Push(State newState)
        {
            Int32 last = _states.Count - 1;

            newState.DoInitialize(this);
            _states.Add(newState);

            if (last >= 0) // has any states
            {
                _states[last].LostFocus(newState);
            }

            _skipDraw = true;
        }

        public void Pop()
        {
            Int32 last = _states.Count - 1;
            State oldState = _states[last];
            _states.RemoveAt(last);

            if (_states.Count > 0)
            {
                Int32 newLast = _states.Count - 1;
                State newState = _states[newLast];

                newState.GotFocus(oldState);
                oldState.Dispose();
                _skipDraw = true;
            }
            else
            {
                Environment.Exit(0);
            }
        }

        public Boolean KeyDown(Keys k, Boolean control, Boolean shift, Boolean alt)
        {
            switch (k)
            {
                case Keys.F8:
                {
                    Runner.Instance.IsMouseVisible = !Runner.Instance.IsMouseVisible;
                    return true;
                }
                case Keys.F7:
                {
                    _showFPS = !_showFPS;
                    return true;
                }
                default:
                {
                    Int32 count = _states.Count;
                    Int32 last = count - 1;

                    for (Int32 i = last; i >= 0; --i)
                    {
                        State s = _states[i];
                        if (s == null) continue;

                        if (s.KeyDown(k, control, shift, alt)) return true;
                        if (s.ShouldSoakInput) break;
                    }
                    return false;
                }
            }

            
        }

        public Boolean KeyUp(Keys k)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.KeyUp(k)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean MouseButtonPressed(MouseButton button, Vector2 position, Boolean control, Boolean shift, Boolean alt)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.MouseButtonPressed(button, position, control, shift, alt)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean MouseButtonReleased(MouseButton button, Vector2 position)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.MouseButtonReleased(button, position)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean MouseScrollWheel(Int32 scrollDirection, Vector2 position)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.MouseScrollWheel(scrollDirection, position)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean MouseMoved(Vector2 position, Vector2 delta)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.MouseMoved(position, delta)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean TouchPressed(Int32 id, Vector2 position)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.TouchPressed(id, position)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean TouchMoved(Int32 id, Vector2 position, Vector2 delta)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.TouchMoved(id, position, delta)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean TouchReleased(Int32 id, Vector2 position)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.TouchReleased(id, position)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean GamePadThumbstickMoved(PlayerIndex index, Thumbstick stick, Vector2 position, Vector2 delta)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadThumbstickMoved(index, stick, position, delta)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean GamePadTriggerMoved(PlayerIndex index, Trigger trigger, Single position, Single delta)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadTriggerMoved(index, trigger, position, delta)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean GamePadEventDown(PlayerIndex index, GamePadEvent eventType)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadEventDown(index, eventType)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public Boolean GamePadEventUp(PlayerIndex index, GamePadEvent eventType)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadEventUp(index, eventType)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public bool GamePadAttached(PlayerIndex index)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadAttached(index)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }

        public bool GamePadDetached(PlayerIndex index)
        {
            Int32 count = _states.Count;
            Int32 last = count - 1;

            for (Int32 i = last; i >= 0; --i)
            {
                State s = _states[i];
                if (s == null) continue;

                if (s.GamePadDetached(index)) return true;
                if (s.ShouldSoakInput) break;
            }
            return false;
        }



        private void RenderSoftwareMouse()
        {
            // TODO: render software mouse once we know what it should be.
        }
    }

    public delegate void PreUpdateDelegate(Double delta);
    public delegate void PostUpdateDelegate(Double delta);

    public delegate void PostDrawDelegate(Double delta, SpriteBatch batch);
}