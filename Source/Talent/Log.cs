﻿using System;

namespace Talent
{
    public static class Log
    {
        private const String LogFormat = "[{0}] {1}: {2}";
        private const String TimeFormat = "HH:mm:ss";

        public enum LoggingLevel
        {
            Trace = 0,
            Debug = 1,
            Info = 2,
            Warn = 3,
            Error = 4
        }

        private const String TraceName = "TRACE";
        private const String DebugName = "DEBUG";
        private const String InfoName = " INFO";
        private const String WarnName = " WARN";
        private const String ErrorName = "ERROR";

        public static LoggingLevel Level { get; set; }
        static Log()
        {
            Level = GetDefaultLevel();
        }


        public static void Initialize()
        {
            Int32 val = Configuration.Get("logging.level", (Int32)Level);
            val = Math.Min((Int32)LoggingLevel.Error, Math.Max((Int32)LoggingLevel.Debug, val));
            Level = (LoggingLevel)val;
        }

        public static void Trace(String text) { Trace(text, String.Empty); }
        public static void Trace(String text, params Object[] args)
        {
            if (Level > LoggingLevel.Trace) return;

            String s = String.Format(text, args);
            String t = String.Format(LogFormat, DateTime.Now.ToString(TimeFormat), TraceName, s);
#if DEBUG
            System.Diagnostics.Debug.Print(t);
#endif
            Console.WriteLine(t);
        }

        public static void Debug(String text) { Debug(text, String.Empty); }
        public static void Debug(String text, params Object[] args)
        {
            if (Level > LoggingLevel.Debug) return;

            String s = String.Format(text, args);
            String t = String.Format(LogFormat, DateTime.Now.ToString(TimeFormat), DebugName, s);
#if DEBUG
            System.Diagnostics.Debug.Print(t);
#endif
            Console.WriteLine(t);
        }

        public static void Info(String text) { Info(text, String.Empty); }
        public static void Info(String text, params Object[] args)
        {
            if (Level > LoggingLevel.Info) return;

            String s = String.Format(text, args);
            String t = String.Format(LogFormat, DateTime.Now.ToString(TimeFormat), InfoName, s);
#if DEBUG
            System.Diagnostics.Debug.Print(t);
#endif
            Console.WriteLine(t);
        }

        public static void Warn(String text) { Warn(text, String.Empty); }
        public static void Warn(String text, params Object[] args)
        {
            if (Level > LoggingLevel.Warn) return;

            String s = String.Format(text, args);
            String t = String.Format(LogFormat, DateTime.Now.ToString(TimeFormat), WarnName, s);
#if DEBUG
            System.Diagnostics.Debug.Print(t);
#endif
            Console.WriteLine(t);
        }

        public static void Error(String text) { Error(text, String.Empty); }
        public static void Error(String text, params Object[] args)
        {
            if (Level > LoggingLevel.Error) return;

            String s = String.Format(text, args);
            String t = String.Format(LogFormat, DateTime.Now.ToString(TimeFormat), ErrorName, s);
#if DEBUG
            System.Diagnostics.Debug.Print(t);
#endif
            Console.WriteLine(t);
        }


        public static LoggingLevel GetDefaultLevel()
        {
#if DEBUG
            return LoggingLevel.Debug;
#else
            return (Debugger.IsAttached) ? LoggingLevel.Debug : LoggingLevel.Info;
#endif
        }
    }
}
