﻿using Microsoft.Xna.Framework;

namespace Talent.Graphics.Sprites
{
    public interface IColorSprite
    {
        Color Color { get; set; }
    }
}
