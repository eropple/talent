﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sprites
{
    public abstract class Sprite
    {
        public Vector2 Position { get; private set; }
        public Double Time { get; private set; }

        public virtual void Update(double delta)
        {
            Time += delta;
        }
        public abstract void Draw(double delta, SpriteBatch batch, Vector2 offset);

        public void Reset(Double newTime = 0)
        {
            Time = newTime;
        }

        public void MoveTo(Vector2 newPosition)
        {
            // TODO: handle collsions for collidable sprites
            Position = newPosition;
        }
        public void MoveBy(Vector2 positionDelta)
        {
            // TODO: handle collisions for collidable sprites
            Position += positionDelta;
        }
    }
}
