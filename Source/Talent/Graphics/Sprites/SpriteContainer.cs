﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sprites
{
    public class SpriteContainer<TSpriteType> : Sprite, IList<TSpriteType>
        where TSpriteType : Sprite
    {
        private readonly List<TSpriteType> _sprites;

        public Rectangle? ChildClippingBounds = null;

        public SpriteContainer(IEnumerable<TSpriteType> sprites = null)
        {
            _sprites = sprites != null ? new List<TSpriteType>(sprites) : new List<TSpriteType>();
        }

        public override void Update(Double delta)
        {
            base.Update(delta);
            DoUpdate(delta);
            foreach(TSpriteType s in _sprites) s.Update(delta);
        }

        public override void Draw(Double delta, SpriteBatch batch, Vector2 offset)
        {
            DoDraw(delta, offset);
            offset += Position;
            if (ChildClippingBounds != null)
            {
                Rectangle savedRect = batch.GraphicsDevice.ScissorRectangle;

                Rectangle cB = (Rectangle) ChildClippingBounds;
                Rectangle scissorRect = new Rectangle((int) (cB.X + Position.X),
                                                      (int) (cB.Y + Position.Y), 
                                                      cB.Width, cB.Height);
                batch.End();
                batch.GraphicsDevice.ScissorRectangle = scissorRect;
                
                batch.BeginDefaults();
                foreach (Sprite s in _sprites) s.Draw(delta, batch, offset);
                batch.End();

                batch.GraphicsDevice.ScissorRectangle = savedRect;
                batch.BeginDefaults();
            }
            else
            {
                foreach (Sprite s in _sprites) s.Draw(delta, batch, offset);
            }
        }

        public virtual void DoUpdate(Double delta) {}
        public virtual void DoDraw(Double delta, Vector2 offset) {}

#region IList<TSpriteType> implementation
        IEnumerator<TSpriteType> IEnumerable<TSpriteType>.GetEnumerator()
        {
            return _sprites.GetEnumerator();
        }

        public TSpriteType this[int index]
        {
            get { return _sprites[index]; }
            set { _sprites[index] = value; }
        }

        public int Count
        {
            get { return _sprites.Count; }
        }

        public bool IsReadOnly { get { return false; } }

        public int Capacity
        {
            get { return _sprites.Capacity; }
            set { _sprites.Capacity = value; }
        }

        public bool TrueForAll(Predicate<TSpriteType> match)
        {
            return _sprites.TrueForAll(match);
        }

        public void TrimExcess()
        {
            _sprites.TrimExcess();
        }

        public TSpriteType[] ToArray()
        {
            return _sprites.ToArray();
        }

        public void Sort(Comparison<TSpriteType> comparison)
        {
            _sprites.Sort(comparison);
        }

        public void Sort(int index, int count, IComparer<TSpriteType> comparer)
        {
            _sprites.Sort(index, count, comparer);
        }

        public void Sort(IComparer<TSpriteType> comparer)
        {
            _sprites.Sort(comparer);
        }

        public void Sort()
        {
            _sprites.Sort();
        }

        public void Reverse(int index, int count)
        {
            _sprites.Reverse(index, count);
        }

        public void Reverse()
        {
            _sprites.Reverse();
        }

        public void RemoveRange(int index, int count)
        {
            _sprites.RemoveRange(index, count);
        }

        public void RemoveAt(int index)
        {
            _sprites.RemoveAt(index);
        }

        public int RemoveAll(Predicate<TSpriteType> match)
        {
            return _sprites.RemoveAll(match);
        }

        public bool Remove(TSpriteType item)
        {
            return _sprites.Remove(item);
        }

        public int LastIndexOf(TSpriteType item, int index, int count)
        {
            return _sprites.LastIndexOf(item, index, count);
        }

        public int LastIndexOf(TSpriteType item, int index)
        {
            return _sprites.LastIndexOf(item, index);
        }

        public int LastIndexOf(TSpriteType item)
        {
            return _sprites.LastIndexOf(item);
        }

        public void InsertRange(int index, IEnumerable<TSpriteType> collection)
        {
            _sprites.InsertRange(index, collection);
        }

        public void Insert(int index, TSpriteType item)
        {
            _sprites.Insert(index, item);
        }

        public int IndexOf(TSpriteType item, int index, int count)
        {
            return _sprites.IndexOf(item, index, count);
        }

        public int IndexOf(TSpriteType item, int index)
        {
            return _sprites.IndexOf(item, index);
        }

        public int IndexOf(TSpriteType item)
        {
            return _sprites.IndexOf(item);
        }

        public List<TSpriteType> GetRange(int index, int count)
        {
            return _sprites.GetRange(index, count);
        }

        public IEnumerator GetEnumerator()
        {
            return _sprites.GetEnumerator();
        }

        public void ForEach(Action<TSpriteType> action)
        {
            _sprites.ForEach(action);
        }

        public int FindLastIndex(int startIndex, int count, Predicate<TSpriteType> match)
        {
            return _sprites.FindLastIndex(startIndex, count, match);
        }

        public int FindLastIndex(int startIndex, Predicate<TSpriteType> match)
        {
            return _sprites.FindLastIndex(startIndex, match);
        }

        public int FindLastIndex(Predicate<TSpriteType> match)
        {
            return _sprites.FindLastIndex(match);
        }

        public TSpriteType FindLast(Predicate<TSpriteType> match)
        {
            return _sprites.FindLast(match);
        }

        public int FindIndex(int startIndex, int count, Predicate<TSpriteType> match)
        {
            return _sprites.FindIndex(startIndex, count, match);
        }

        public int FindIndex(int startIndex, Predicate<TSpriteType> match)
        {
            return _sprites.FindIndex(startIndex, match);
        }

        public int FindIndex(Predicate<TSpriteType> match)
        {
            return _sprites.FindIndex(match);
        }

        public List<TSpriteType> FindAll(Predicate<TSpriteType> match)
        {
            return _sprites.FindAll(match);
        }

        public TSpriteType Find(Predicate<TSpriteType> match)
        {
            return _sprites.Find(match);
        }

        public bool Exists(Predicate<TSpriteType> match)
        {
            return _sprites.Exists(match);
        }

        public void CopyTo(TSpriteType[] array, int arrayIndex)
        {
            _sprites.CopyTo(array, arrayIndex);
        }

        public void CopyTo(int index, TSpriteType[] array, int arrayIndex, int count)
        {
            _sprites.CopyTo(index, array, arrayIndex, count);
        }

        public void CopyTo(TSpriteType[] array)
        {
            _sprites.CopyTo(array);
        }

        public List<TOutput> ConvertAll<TOutput>(Converter<TSpriteType, TOutput> converter)
        {
            return _sprites.ConvertAll(converter);
        }

        public bool Contains(TSpriteType item)
        {
            return _sprites.Contains(item);
        }

        public void Clear()
        {
            _sprites.Clear();
        }

        public int BinarySearch(TSpriteType item, IComparer<TSpriteType> comparer)
        {
            return _sprites.BinarySearch(item, comparer);
        }

        public int BinarySearch(TSpriteType item)
        {
            return _sprites.BinarySearch(item);
        }

        public int BinarySearch(int index, int count, TSpriteType item, IComparer<TSpriteType> comparer)
        {
            return _sprites.BinarySearch(index, count, item, comparer);
        }

        public ReadOnlyCollection<TSpriteType> AsReadOnly()
        {
            return _sprites.AsReadOnly();
        }

        public void AddRange(IEnumerable<TSpriteType> collection)
        {
            _sprites.AddRange(collection);
        }

        public void Add(TSpriteType item)
        {
            _sprites.Add(item);
        }
#endregion
    }
}
