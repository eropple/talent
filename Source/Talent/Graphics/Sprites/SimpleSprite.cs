﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Talent.Graphics.Sources;

namespace Talent.Graphics.Sprites
{
    public class SimpleSprite : Sprite, IColorSprite
    {
        public readonly IDrawSource Source;
        public Color Color { get; set; }

        public SimpleSprite(IDrawSource source)
        {
            Source = source;
            Color = Color.White;
        }

        public override void Draw(Double delta, SpriteBatch batch, Vector2 offset)
        {
            Vector2 spot = offset + Position;

            Texture2D tex;
            Rectangle rect;
            Source.GetDrawable(Time, out tex, out rect);

            batch.Draw(tex, spot, rect, Color);
        }
    }
}
