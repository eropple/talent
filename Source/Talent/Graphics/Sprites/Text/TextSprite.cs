﻿using System;
using Talent.Graphics.Fonts.Spec;

namespace Talent.Graphics.Sprites.Text
{
    public class TextSprite : SpriteContainer<TextLineSprite>
    {
        public readonly FontSpec Font;

        public TextSprite(FontSpec font)
        {
            Font = font;

        }

        public Int32 Height
        {
            get { return Count*Font.LineHeight; }
        }
    }
}
