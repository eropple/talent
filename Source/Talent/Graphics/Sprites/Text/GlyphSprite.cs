﻿using Talent.Graphics.Fonts;
using Talent.Graphics.Fonts.Spec;

namespace Talent.Graphics.Sprites.Text
{
    public class GlyphSprite : SimpleSprite
    {
        public readonly CharacterInfo CharacterInfo;

        public GlyphSprite(TextBuilder builder, CharacterInfo charInfo) : base(builder.Glyphs[charInfo.ID])
        {
            CharacterInfo = charInfo;
        }
    }
}
