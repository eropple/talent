﻿using System;
using System.Collections;
using System.Collections.Generic;
using Talent.Graphics.Sprites;

namespace Talent.Graphics.Tweening
{
    public class TweenContainer : ITweener, IDictionary<String, ITweener>
    {
        private readonly IDictionary<String, ITweener> _dictionary; 

        public TweenContainer()
        {
            _dictionary = new SortedDictionary<String, ITweener>();
        }

        public void Tween(Sprite sprite, Double timeOffset = 0.0f)
        {
            foreach (var kvp in _dictionary) kvp.Value.Tween(sprite, timeOffset);
        }

        public IEnumerator<KeyValuePair<string, ITweener>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        public void Add(KeyValuePair<string, ITweener> item)
        {
            _dictionary.Add(item);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        public bool Contains(KeyValuePair<string, ITweener> item)
        {
            return _dictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<string, ITweener>[] array, int arrayIndex)
        {
            _dictionary.CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<string, ITweener> item)
        {
            return _dictionary.Remove(item);
        }

        public int Count
        {
            get { return _dictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return _dictionary.IsReadOnly; }
        }

        public bool ContainsKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public void Add(string key, ITweener value)
        {
            _dictionary.Add(key, value);
        }

        public bool Remove(string key)
        {
            return _dictionary.Remove(key);
        }

        public bool TryGetValue(string key, out ITweener value)
        {
            return _dictionary.TryGetValue(key, out value);
        }

        public ITweener this[string key]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = value; }
        }

        public ICollection<string> Keys
        {
            get { return _dictionary.Keys; }
        }

        public ICollection<ITweener> Values
        {
            get { return _dictionary.Values; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
