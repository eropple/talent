﻿using System;
using Talent.Graphics.Sprites;

namespace Talent.Graphics.Tweening
{
    public delegate void TweenCompleteDelegate(ITweener tweener);

    public interface ITweener
    {
        void Tween(Sprite sprite, Double timeOffset = 0.0f);
    }
}
