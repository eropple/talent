﻿using System;
using Talent.Graphics.Sprites;
using Talent.Utils;

namespace Talent.Graphics.Tweening
{
    public abstract class BaseTweener : ITweener
    {
        public readonly Double Duration;
        public readonly InterpolationFunction Function;
        private readonly TweenCompleteDelegate OnFinish;

        protected BaseTweener(Double duration, InterpolationFunction function, TweenCompleteDelegate onFinish = null)
        {
            Duration = duration;
            Function = function;
            OnFinish = onFinish;
        }

        public void Tween(Sprite sprite, Double timeOffset = 0.0f)
        {
            Double t = MoreMath.Clamp(sprite.Time - timeOffset, 0.0, Duration);
            Double p = Function(t, 0.0, 1.0, Duration);

            DoTween(sprite, p);

            if (p.Equals(1.0))
            {
                OnFinish(this);
            }
        }

        /// <summary>
        /// Called to actually perform a given tweening operation.
        /// </summary>
        /// <param name="proportion">
        /// A value between 0.0 and 1.0, 
        /// </param>
        protected abstract void DoTween(Sprite sprite, Double proportion);
    }
}
