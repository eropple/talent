﻿using System;

namespace Talent.Graphics.Tweening
{
    /// <summary>
    /// A pure function that describes an interpolation curve.
    /// </summary>
    /// <param name="currentTime">The current time, beginning at zero.</param>
    /// <param name="startValue">The starting value for this interpolation.</param>
    /// <param name="endValue">The ending value for this interpolation.</param>
    /// <param name="duration">The length of the interpolation (the end time).</param>
    /// <returns></returns>
    public delegate Double InterpolationFunction(Double currentTime, Double startValue,
                                                 Double endValue, Double duration);

    public static class Interpolations
    {
        // http://wpf-animation.googlecode.com/svn/trunk/src/WPF/Animation/PennerDoubleAnimation.cs
        // TODO: BSD license, make sure to properly credit.

        #region Equations

        #region Linear

        /// <summary>
        /// Easing equation function for a simple linear tweening, with no easing.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double Linear(Double t, Double b, Double c, Double d)
        {
            return c*t/d + b;
        }

        #endregion

        #region Expo

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ExpoEaseOut(Double t, Double b, Double c, Double d)
        {
            return (t == d) ? b + c : c*(-Math.Pow(2, -10*t/d) + 1) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ExpoEaseIn(Double t, Double b, Double c, Double d)
        {
            return (t == 0) ? b : c*Math.Pow(2, 10*(t/d - 1)) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ExpoEaseInOut(Double t, Double b, Double c, Double d)
        {
            if (t == 0)
                return b;

            if (t == d)
                return b + c;

            if ((t /= d/2) < 1)
                return c/2*Math.Pow(2, 10*(t - 1)) + b;

            return c/2*(-Math.Pow(2, -10*--t) + 2) + b;
        }

        /// <summary>
        /// Easing equation function for an exponential (2^t) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ExpoEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return ExpoEaseOut(t*2, b, c/2, d);

            return ExpoEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Circular

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CircEaseOut(Double t, Double b, Double c, Double d)
        {
            return c*Math.Sqrt(1 - (t = t/d - 1)*t) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CircEaseIn(Double t, Double b, Double c, Double d)
        {
            return -c*(Math.Sqrt(1 - (t /= d)*t) - 1) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CircEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return -c/2*(Math.Sqrt(1 - t*t) - 1) + b;

            return c/2*(Math.Sqrt(1 - (t -= 2)*t) + 1) + b;
        }

        /// <summary>
        /// Easing equation function for a circular (sqrt(1-t^2)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CircEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return CircEaseOut(t*2, b, c/2, d);

            return CircEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Quad

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuadEaseOut(Double t, Double b, Double c, Double d)
        {
            return -c*(t /= d)*(t - 2) + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuadEaseIn(Double t, Double b, Double c, Double d)
        {
            return c*(t /= d)*t + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuadEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return c/2*t*t + b;

            return -c/2*((--t)*(t - 2) - 1) + b;
        }

        /// <summary>
        /// Easing equation function for a quadratic (t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuadEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return QuadEaseOut(t*2, b, c/2, d);

            return QuadEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Sine

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double SineEaseOut(Double t, Double b, Double c, Double d)
        {
            return c*Math.Sin(t/d*(Math.PI/2)) + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double SineEaseIn(Double t, Double b, Double c, Double d)
        {
            return -c*Math.Cos(t/d*(Math.PI/2)) + c + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double SineEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return c/2*(Math.Sin(Math.PI*t/2)) + b;

            return -c/2*(Math.Cos(Math.PI*--t/2) - 2) + b;
        }

        /// <summary>
        /// Easing equation function for a sinusoidal (sin(t)) easing in/out: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double SineEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return SineEaseOut(t*2, b, c/2, d);

            return SineEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Cubic

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CubicEaseOut(Double t, Double b, Double c, Double d)
        {
            return c*((t = t/d - 1)*t*t + 1) + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CubicEaseIn(Double t, Double b, Double c, Double d)
        {
            return c*(t /= d)*t*t + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CubicEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return c/2*t*t*t + b;

            return c/2*((t -= 2)*t*t + 2) + b;
        }

        /// <summary>
        /// Easing equation function for a cubic (t^3) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double CubicEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return CubicEaseOut(t*2, b, c/2, d);

            return CubicEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Quartic

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuartEaseOut(Double t, Double b, Double c, Double d)
        {
            return -c*((t = t/d - 1)*t*t*t - 1) + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuartEaseIn(Double t, Double b, Double c, Double d)
        {
            return c*(t /= d)*t*t*t + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuartEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return c/2*t*t*t*t + b;

            return -c/2*((t -= 2)*t*t*t - 2) + b;
        }

        /// <summary>
        /// Easing equation function for a quartic (t^4) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuartEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return QuartEaseOut(t*2, b, c/2, d);

            return QuartEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Quintic

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuintEaseOut(Double t, Double b, Double c, Double d)
        {
            return c*((t = t/d - 1)*t*t*t*t + 1) + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuintEaseIn(Double t, Double b, Double c, Double d)
        {
            return c*(t /= d)*t*t*t*t + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuintEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) < 1)
                return c/2*t*t*t*t*t + b;
            return c/2*((t -= 2)*t*t*t*t + 2) + b;
        }

        /// <summary>
        /// Easing equation function for a quintic (t^5) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double QuintEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return QuintEaseOut(t*2, b, c/2, d);
            return QuintEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Elastic

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ElasticEaseOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d) == 1)
                return b + c;

            Double p = d*.3;
            Double s = p/4;

            return (c*Math.Pow(2, -10*t)*Math.Sin((t*d - s)*(2*Math.PI)/p) + c + b);
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ElasticEaseIn(Double t, Double b, Double c, Double d)
        {
            if ((t /= d) == 1)
                return b + c;

            Double p = d*.3;
            Double s = p/4;

            return -(c*Math.Pow(2, 10*(t -= 1))*Math.Sin((t*d - s)*(2*Math.PI)/p)) + b;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ElasticEaseInOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d/2) == 2)
                return b + c;

            Double p = d*(.3*1.5);
            Double s = p/4;

            if (t < 1)
                return -.5*(c*Math.Pow(2, 10*(t -= 1))*Math.Sin((t*d - s)*(2*Math.PI)/p)) + b;
            return c*Math.Pow(2, -10*(t -= 1))*Math.Sin((t*d - s)*(2*Math.PI)/p)*.5 + c + b;
        }

        /// <summary>
        /// Easing equation function for an elastic (exponentially decaying sine wave) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double ElasticEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return ElasticEaseOut(t*2, b, c/2, d);
            return ElasticEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Bounce

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BounceEaseOut(Double t, Double b, Double c, Double d)
        {
            if ((t /= d) < (1/2.75))
                return c*(7.5625*t*t) + b;
            else if (t < (2/2.75))
                return c*(7.5625*(t -= (1.5/2.75))*t + .75) + b;
            else if (t < (2.5/2.75))
                return c*(7.5625*(t -= (2.25/2.75))*t + .9375) + b;
            else
                return c*(7.5625*(t -= (2.625/2.75))*t + .984375) + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BounceEaseIn(Double t, Double b, Double c, Double d)
        {
            return c - BounceEaseOut(d - t, 0, c, d) + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BounceEaseInOut(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return BounceEaseIn(t*2, 0, c, d)*.5 + b;
            else
                return BounceEaseOut(t*2 - d, 0, c, d)*.5 + c*.5 + b;
        }

        /// <summary>
        /// Easing equation function for a bounce (exponentially decaying parabolic bounce) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BounceEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return BounceEaseOut(t*2, b, c/2, d);
            return BounceEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #region Back

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out: 
        /// decelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BackEaseOut(Double t, Double b, Double c, Double d)
        {
            return c*((t = t/d - 1)*t*((1.70158 + 1)*t + 1.70158) + 1) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in: 
        /// accelerating from zero velocity.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BackEaseIn(Double t, Double b, Double c, Double d)
        {
            return c*(t /= d)*t*((1.70158 + 1)*t - 1.70158) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing in/out: 
        /// acceleration until halfway, then deceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BackEaseInOut(Double t, Double b, Double c, Double d)
        {
            Double s = 1.70158;
            if ((t /= d/2) < 1)
                return c/2*(t*t*(((s *= (1.525)) + 1)*t - s)) + b;
            return c/2*((t -= 2)*t*(((s *= (1.525)) + 1)*t + s) + 2) + b;
        }

        /// <summary>
        /// Easing equation function for a back (overshooting cubic easing: (s+1)*t^3 - s*t^2) easing out/in: 
        /// deceleration until halfway, then acceleration.
        /// </summary>
        /// <param name="t">Current time in seconds.</param>
        /// <param name="b">Starting value.</param>
        /// <param name="c">Final value.</param>
        /// <param name="d">Duration of animation.</param>
        /// <returns>The correct value.</returns>
        public static Double BackEaseOutIn(Double t, Double b, Double c, Double d)
        {
            if (t < d/2)
                return BackEaseOut(t*2, b, c/2, d);
            return BackEaseIn((t*2) - d, b + c/2, c/2, d);
        }

        #endregion

        #endregion
    }
}
