﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sources
{
    public class AtlasSource : IReadOnlyDictionary<String, TextureSliceSource>
    {
        private readonly Texture2D _texture;
        private readonly IReadOnlyDictionary<String, TextureSliceSource> _entries;

        public AtlasSource(Texture2D texture, IEnumerable<KeyValuePair<String, Rectangle>> entries)
        {
            _texture = texture;
            _entries = entries.ToDictionary(kvp => kvp.Key, 
                                            kvp => new TextureSliceSource(texture, kvp.Value));
        }

        public IEnumerator<KeyValuePair<String, TextureSliceSource>> GetEnumerator()
        {
            return _entries.GetEnumerator();
        }

        public int Count
        {
            get { return _entries.Count; }
        }

        public bool ContainsKey(String key)
        {
            return _entries.ContainsKey(key);
        }

        public bool TryGetValue(string key, out TextureSliceSource value)
        {
            return _entries.TryGetValue(key, out value);
        }

        public TextureSliceSource this[string key]
        {
            get { return _entries[key]; }
        }

        public IEnumerable<string> Keys
        {
            get { return _entries.Keys; }
        }

        public IEnumerable<TextureSliceSource> Values
        {
            get { return _entries.Values; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
