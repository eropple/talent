﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sources
{
    public class TextureSliceSource : IDrawSource
    {
        public readonly Texture2D Texture;
        public readonly Rectangle Rect;

        public TextureSliceSource(Texture2D texture, Rectangle rect)
        {
            Texture = texture;
            Rect = rect;
        }

        public void GetDrawable(Double time, out Texture2D texture, out Rectangle rect)
        {
            texture = Texture;
            rect = Rect;
        }
    }
}
