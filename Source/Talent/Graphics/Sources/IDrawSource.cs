﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sources
{
    public interface IDrawSource
    {
        void GetDrawable(Double time, out Texture2D texture, out Rectangle rect); // lets us save a return-and-store
    }
}
