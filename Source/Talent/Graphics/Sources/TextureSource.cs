﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sources
{
    public class TextureSource : IDrawSource
    {
        public readonly Texture2D Texture;

        public TextureSource(Texture2D texture)
        {
            Texture = texture;
        }

        public void GetDrawable(Double time, out Texture2D texture, out Rectangle rect)
        {
            texture = Texture;
            rect = Texture.Bounds;
        }

        public TextureSliceSource GetSlice(Rectangle rect)
        {
            if (!Texture.Bounds.Contains(rect))
            {
                throw new ArgumentOutOfRangeException("Specified slice rect is not within texture bounds.");
            }

            return new TextureSliceSource(this, rect);
        }



        public static implicit operator Texture2D(TextureSource t)
        {
            return t.Texture;
        }
    }
}
