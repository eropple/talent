﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent.Graphics.Sources
{
    public class AnimationSource : IDrawSource
    {
        public readonly Int32 FrameDurationInMillis;
        public readonly IReadOnlyList<TextureSliceSource> Frames;

        private readonly Int32 _frameTimeTotal;

        public AnimationSource(Int32 frameDurationInMillis, IEnumerable<TextureSliceSource> frames)
        {
            FrameDurationInMillis = frameDurationInMillis;
            Frames = new List<TextureSliceSource>(frames);
            _frameTimeTotal = FrameDurationInMillis*Frames.Count;
        }


        public void GetDrawable(Double time, out Texture2D texture, out Rectangle rect)
        {
            Int32 i = ((Int32)(time * 1000) % _frameTimeTotal) / FrameDurationInMillis;
            Frames[i].GetDrawable(0.0, out texture, out rect);
        }
    }
}
