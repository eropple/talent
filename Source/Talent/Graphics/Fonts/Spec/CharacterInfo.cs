﻿using System;
using Microsoft.Xna.Framework;
using Talent.Graphics.Fonts.BMFont;

namespace Talent.Graphics.Fonts.Spec
{
    public class CharacterInfo
    {
        public readonly Int32 ID;
        public readonly Point Position;
        public readonly Point Size;
        public readonly Point Offset;
        public readonly Int32 XAdvance;
        public readonly Int32 Page;

        public CharacterInfo(FontChar c)
        {
            this.ID = c.ID;
            this.Position = new Point(c.X, c.Y);
            this.Size = new Point(c.Width, c.Height);
            this.Offset = new Point(c.XOffset, c.YOffset);
            this.XAdvance = c.XAdvance;
            this.Page = c.Page;
        }
    }
}