﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Talent.Graphics.Fonts.BMFont;

namespace Talent.Graphics.Fonts.Spec
{
    /// <summary>
    /// Translation object to turn the BMFont stuff into a somewhat
    /// sane object format for our use.
    /// </summary>
    public class FontSpec
    {
        public readonly String Face;
        public readonly Int32 Size;
        public readonly Boolean Bold;
        public readonly Boolean Italic;
        public readonly String Encoding;
        public readonly Boolean Unicode;

        public readonly Int32 StretchHeight;
        public readonly Boolean Smoothed;
        public readonly Boolean SuperSampled;
        public readonly Rectangle Padding;
        public readonly Point Spacing;
        public readonly Boolean Outline;

        public readonly Int32 LineHeight;
        public readonly Int32 Baseline;
        public readonly Point TextureSize;
        public readonly IReadOnlyDictionary<Int32, String> TexturePageInfo;

        public readonly IReadOnlyDictionary<Int32, CharacterInfo> CharacterTable;

        public readonly IReadOnlyDictionary<Int32, IReadOnlyDictionary<Int32, KerningInfo>> KerningTable;

        public FontSpec(FontFile font)
        {
            this.Face = font.Info.Face;
            this.Size = font.Info.Size;
            this.Bold = font.Info.Bold == 1;
            this.Italic = font.Info.Italic == 1;
            this.Encoding = font.Info.CharSet;
            this.Unicode = font.Info.Unicode == 1;

            this.StretchHeight = font.Info.StretchHeight;
            this.Smoothed = font.Info.Smooth == 1;
            this.SuperSampled = font.Info.SuperSampling == 1;
            this.Padding = font.Info._Padding;
            this.Spacing = font.Info._Spacing;
            this.Outline = font.Info.OutLine == 1;


            this.LineHeight = font.Common.LineHeight;
            this.Baseline = font.Common.Base;
            this.TextureSize = new Point(font.Common.ScaleW, font.Common.ScaleH);

            this.TexturePageInfo = font.Pages.ToDictionary(p => p.ID, p => p.File);

            this.CharacterTable = font.Chars.ToDictionary(c => c.ID, c => new CharacterInfo(c));

            var kernDict = new Dictionary<Int32, Dictionary<Int32, KerningInfo>>();
            foreach (FontKerning k in font.Kernings)
            {
                Dictionary<Int32, KerningInfo> subDict = null;

                if (kernDict.TryGetValue(k.First, out subDict) == false)
                {
                    subDict = new Dictionary<Int32, KerningInfo>();
                    kernDict.Add(k.First, subDict);
                }

                subDict.Add(k.Second, new KerningInfo(k));
            }
            this.KerningTable = kernDict.Keys.ToDictionary<int, int, IReadOnlyDictionary<int, KerningInfo>>(k => k, k => kernDict[k]);
        }

        public Int32? GetKerningValue(Int32 a, Int32 b)
        {
            IReadOnlyDictionary<Int32, KerningInfo> subDict = null;

            if (this.KerningTable.TryGetValue(a, out subDict) == false)
            {
                return null;
            }

            KerningInfo k = null;

            if (subDict.TryGetValue(b, out k) == false)
            {
                return null;
            }

            return k.Amount;
        }
    }
}