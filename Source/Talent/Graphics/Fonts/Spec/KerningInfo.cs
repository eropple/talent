﻿using System;
using Talent.Graphics.Fonts.BMFont;

namespace Talent.Graphics.Fonts.Spec
{
    public class KerningInfo
    {
        public readonly Int32 First;
        public readonly Int32 Second;
        public readonly Int32 Amount;

        public KerningInfo(FontKerning k)
        {
            this.First = k.First;
            this.Second = k.Second;
            this.Amount = k.Amount;
        }
    }
}