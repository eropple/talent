﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Talent.Graphics.Fonts.Spec;
using Talent.Graphics.Sources;
using Talent.Graphics.Sprites.Text;

namespace Talent.Graphics.Fonts
{
    public class TextBuilder
    {
        public readonly FontSpec Spec;
        public readonly IReadOnlyDictionary<Int32, TextureSliceSource> Glyphs; 

        public TextBuilder(FontSpec spec, IDictionary<Int32, TextureSource> textures)
        {
            Spec = spec;

            var glyphs = new Dictionary<Int32, TextureSliceSource>();
            foreach (var kvp in spec.CharacterTable)
            {
                var pos = kvp.Value.Position;
                var size = kvp.Value.Size;

                var rect = new Rectangle(pos.X, pos.Y, size.X, size.Y);
                var slice = textures[kvp.Value.Page].GetSlice(rect);

                glyphs.Add(kvp.Key, slice);
            }
            Glyphs = glyphs;
        }

        public TextSprite BuildTextSprite(String text, Color defaultColor)
        {
            return BuildTextSprite(text, defaultColor, -1);
        }
        public TextSprite BuildTextSprite(String text, Color? defaultColor = null, Int32 wrapWidth = -1)
        {
            TextSprite anchor = new TextSprite(Spec);

            var lines = BuildGlyphSet(text, wrapWidth, defaultColor ?? Color.White);

            for (Int32 i = 0; i < lines.Count; ++i)
            {
                TextLineSprite line = BuildLineFromGlyphs(lines[i]);
                line.MoveTo(new Vector2(0, i * Spec.LineHeight));
                anchor.Add(line);
            }

            return anchor;
        }

        // TODO: this can almost certainly be unrolled into BuildTextSprite but that'll require lookahead
        protected List<List<GlyphInfo>> BuildGlyphSet(String text, Int32 wrapWidth, Color defaultColor)
        {
            Color currentColor = defaultColor;

            StringBuilder work = new StringBuilder(text);

            List<List<GlyphInfo>> lines = new List<List<GlyphInfo>>();

            List<GlyphInfo> line = new List<GlyphInfo>();
            Int32 lineLength = 0;
            List<GlyphInfo> word = new List<GlyphInfo>();
            Int32 wordLength = 0;

            while (work.Length > 0)
            {
                String code = null;

                if (ControlCheck(ref work, defaultColor, ref currentColor)) continue;

                Char c = work[0];

                if (c == '\n')
                {
                    work.Remove(0, 1);
                    
                    line.AddRange(word);
                    lines.Add(line);
                    word = new List<GlyphInfo>();
                    line = new List<GlyphInfo>();
                    lineLength = 0;
                    wordLength = 0;
                    continue;
                }

                CharacterInfo charInfo = this.Spec.CharacterTable[Convert.ToInt32(c)];


                word.Add(new GlyphInfo(c, charInfo, currentColor));
                wordLength += charInfo.XAdvance;

                if (c == ' ')
                {
                    if (wrapWidth != -1) // word wrapping is on
                    {
                        if (wordLength + lineLength > wrapWidth)
                        {
                            lines.Add(line); // old line is archived

                            line = word; // word becomes the new line (start of new line)
                            lineLength = wordLength; // carry over the value
                        }
                        else
                        {
                            line.AddRange(word);
                            lineLength += wordLength;
                        }

                        word = new List<GlyphInfo>();  // reset for next word
                        wordLength = 0;
                    }
                    else
                    {
                        // no word wrapping, just add to list
                        line.AddRange(word);
                        lineLength += wordLength;

                        word = new List<GlyphInfo>();  // reset for next word
                        wordLength = 0;
                    }
                }

                work.Remove(0, 1);
            }

            if (wrapWidth != -1) // word wrapping is on
            {
                if (wordLength + lineLength > wrapWidth)
                {
                    lines.Add(line); // old line is archived

                    line = word; // word becomes the new line (start of new line)
                    lineLength = wordLength; // carry over the value
                }
                else
                {
                    line.AddRange(word);
                    lineLength += wordLength;
                }

                word = new List<GlyphInfo>();  // reset for next word
                wordLength = 0;
            }
            else
            {
                line.AddRange(word);
                lineLength += wordLength;
            }

            lines.Add(line);

            return lines;
        }

        protected TextLineSprite BuildLineFromGlyphs(List<GlyphInfo> line)
        {
            TextLineSprite anchor = new TextLineSprite();

            Int32 xpos = 0;

            for (Int32 i = 0; i < line.Count; ++i)
            {
                GlyphInfo g = line[i];

                GlyphSprite glyph = new GlyphSprite(this, g.CharacterInfo) { Color = g.Color };

                Int32? kern = i == 0 ? null : Spec.GetKerningValue(line[i - 1].CharacterInfo.ID, g.CharacterInfo.ID);
                Int32 k = kern != null ? (Int32) kern : 0;

                glyph.MoveTo(new Vector2(xpos + k + g.CharacterInfo.Offset.X, 0 + g.CharacterInfo.Offset.Y));

                anchor.Add(glyph);

                xpos += g.CharacterInfo.XAdvance + k;
            }

            return anchor;
        }

        protected static readonly Regex ShortColorCodeRegex = new Regex(@"^\{\#[a-fA-F0-9]{6}\}", RegexOptions.IgnoreCase);
        protected static readonly Regex ColorCodeRegex = new Regex(@"^\{\#[a-fA-F0-9]{8}\}", RegexOptions.IgnoreCase);
        protected static readonly Regex ColorResetRegex = new Regex(@"^\{\#x\}", RegexOptions.IgnoreCase);
        protected static Boolean ControlCheck(ref StringBuilder work, Color defaultColor, ref Color currentColor)
        {
            String w = work.ToString();
            if (ColorCodeRegex.IsMatch(w))
            {
                var code = w.Substring(0, 11);

                Byte r = Byte.Parse(code.Substring(2, 2), NumberStyles.HexNumber);
                Byte g = Byte.Parse(code.Substring(4, 2), NumberStyles.HexNumber);
                Byte b = Byte.Parse(code.Substring(6, 2), NumberStyles.HexNumber);
                Byte a = Byte.Parse(code.Substring(8, 2), NumberStyles.HexNumber);

                currentColor = new Color(r, g, b, a);
                work.Remove(0, 11);
                return true;
            }
            if (ShortColorCodeRegex.IsMatch(w))
            {
                var code = w.Substring(0, 9);

                Byte r = Byte.Parse(code.Substring(2, 2), NumberStyles.HexNumber);
                Byte g = Byte.Parse(code.Substring(4, 2), NumberStyles.HexNumber);
                Byte b = Byte.Parse(code.Substring(6, 2), NumberStyles.HexNumber);

                currentColor = new Color(r, g, b, Byte.MaxValue);
                work.Remove(0, 9);
                return true;
            }
            if (ColorResetRegex.IsMatch(w))
            {
                currentColor = defaultColor;
                work.Remove(0, 4);
                return true;
            }

            return false;
        }
    }
}
