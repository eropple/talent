﻿using System;
using Microsoft.Xna.Framework;
using Talent.Graphics.Fonts.Spec;

namespace Talent.Graphics.Fonts
{
    public class GlyphInfo
    {
        public readonly Char Character;
        public readonly CharacterInfo CharacterInfo;
        public readonly Color Color;

        public GlyphInfo(Char c, CharacterInfo info, Color color)
        {
            this.Character = c;
            this.CharacterInfo = info;
            this.Color = color;
        }

        public override string ToString()
        {
            return Character.ToString() + " " + Color.ToString();
        }
    }
}