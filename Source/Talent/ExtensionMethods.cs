﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Talent
{
    public static class ExtensionMethods
    {
        public static Boolean Contains(this Rectangle rect, Vector2 vector)
        {
            return !(vector.X < rect.Left || vector.X > rect.Right || vector.Y < rect.Top || vector.Y > rect.Bottom);
        }

        private static readonly RasterizerState _defaultState = new RasterizerState()
        {
            CullMode = CullMode.CullCounterClockwiseFace,
            ScissorTestEnable = true
        };

        public static void BeginDefaults(this SpriteBatch batch)
        {
            batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, null, null, _defaultState);
        }



        public static Stream ToStream(this String str)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}