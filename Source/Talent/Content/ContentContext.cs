﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Anaeax.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Scripting.SSharp;
using SharpFileSystem;
using Talent.Graphics.Fonts;
using Talent.Graphics.Sources;

namespace Talent.Content
{
    public class ContentContext : IDisposable
    {
        private static Int32 _nextContextID = 0;
        private static readonly IReadOnlyDictionary<Type, ContentParserDelegate> ParserMap;
        static ContentContext()
        {
            ParserMap = new Dictionary<Type, ContentParserDelegate>
                            {
                                { typeof(String),           BuiltinParsers.GetString },
                                { typeof(Texture2D),        BuiltinParsers.GetTexture2D },
                                { typeof(TextureSource),    BuiltinParsers.GetTextureSource },
                                { typeof(AtlasSource),      BuiltinParsers.GetAtlasSource },
                                { typeof(TextBuilder),      BuiltinParsers.GetTextBuilder },
                                { typeof(AnimationSource),  BuiltinParsers.GetAnimationSource },
                                { typeof(Script),           BuiltinParsers.GetScript }
                            };
        }


        public readonly Int32 ID;
        public readonly GraphicsDeviceManager GraphicsDeviceManager;

        private readonly ContentContext _parent;
        private readonly Dictionary<Type, Dictionary<String, Object>> _loadedObjects;
        

        private readonly IReadOnlyDictionary<Type, ContentParserDelegate> _contextParsers; 

        public ContentContext(GraphicsDeviceManager graphics, Boolean useCustomContentParsers,
            IReadOnlyDictionary<Type, ContentParserDelegate> customParsers = null)
        {
            if (!useCustomContentParsers || customParsers == null || customParsers.Count == 0)
            {
                _contextParsers = ParserMap;
            }
            else
            {
                _contextParsers = customParsers.Concat(ParserMap).ToDictionary(k => k.Key, v => v.Value);
            }

            GraphicsDeviceManager = graphics;

            ID = 0;

            _nextContextID = 1;

            _loadedObjects = BuildObjectDictionary();

            Log.Trace("Base content context created.");
        }

        public ContentContext(ContentContext parent)
        {
            _parent = parent;

            ID = Interlocked.Increment(ref _nextContextID) - 1;

            Log.Trace("Content context #{0} created, parent is #{1}.",
                ID, parent.ID);

            _contextParsers = parent._contextParsers;
            GraphicsDeviceManager = parent.GraphicsDeviceManager;

            _loadedObjects = BuildObjectDictionary();
        }


        public Boolean IsDisposed { get; private set; }
        public void Dispose()
        {
            if (IsDisposed) return;
            IsDisposed = true;

            Log.Debug("Disposing content context #{0}.", ID);

            foreach (var obj in _loadedObjects
                                    .Where(kvp => typeof(IDisposable).IsAssignableFrom(kvp.Key))
                                    .SelectMany(kvp => kvp.Value.Values))
            {
                ((IDisposable)obj).Dispose();
            }

            GC.SuppressFinalize(this);
        }

        public TContentType Load<TContentType>(String path)
            where TContentType : class
        {
            path = path.Trim().TrimStart(new[] { '/' });

            TContentType item = GetIfLoaded<TContentType>(path);
            if (item != null)
            {
                Log.Trace("Content context #{0}: found '{1}' of type '{2}",
                          ID, path, typeof(TContentType));
                return item;
            }

            if (_parent != null)
            {
                item = _parent.GetIfLoaded<TContentType>(path);
                if (item != null)
                {
                    Log.Trace("Content context #{0}: parent had '{1}' of type '{2}",
                              ID, path, typeof(TContentType));
                }
            }

            item = LoadUnmanaged<TContentType>(path);
            Store(item, path); // LoadUnmanaged throws on null, no check needed
            return item;
        }


        public TContentType LoadUnmanaged<TContentType>(String path)
            where TContentType : class
        {
            path = path.Trim().TrimStart(new[] { '/' });
            Type cType = typeof(TContentType);

            ContentParserDelegate del;
            if (!_contextParsers.TryGetValue(cType, out del))
            {
                throw new ArgumentOutOfRangeException(String.Format("There is no content loader for type '{0}'.", cType));
            }

            FileSystemPath fsPath = FileSystemPath.Root.AppendPath(path);
            if (!Files.GameData.Exists(fsPath)) throw new FileNotFoundException(path);

            TContentType item = del(fsPath, this) as TContentType;

            if (item == null)
            {
                Log.Error("Content context #{0}: failed to load '{1}' of type '{2}",
                              ID, path, typeof(TContentType));
                throw new InvalidDataException("Content parser returned null or object not of type '" + cType + "'.");
            }

            Log.Trace("Content context #{0}: loaded '{1}' of type '{2}",
                              ID, path, typeof(TContentType));

            return item;
        }

        private TContentType GetIfLoaded<TContentType>(String path)
            where TContentType : class
        {
            Type cType = typeof(TContentType);
            Dictionary<String, Object> dict;
            if (!_loadedObjects.TryGetValue(cType, out dict))
            {
                throw new ArgumentOutOfRangeException(String.Format("There is no content dictionary for type '{0}'.", cType));
            }
            Object item = null;
            return (dict.TryGetValue(path, out item)) ? (TContentType)item : null;
        }

        private void Store<TContentType>(TContentType item, String path)
        {
            Type cType = typeof(TContentType);
            Dictionary<String, Object> dict;
            if (!_loadedObjects.TryGetValue(cType, out dict))
            {
                throw new ArgumentOutOfRangeException(String.Format("There is no content dictionary for type '{0}'.", cType));
            }
            dict.Add(path, item);

            Log.Trace("Content context #{0}: stored '{1}' of type '{2}",
                              ID, path, typeof(TContentType));
        }



        private Dictionary<Type, Dictionary<String, Object>> BuildObjectDictionary()
        {
            var dict = new Dictionary<Type, Dictionary<String, Object>>();
            foreach (Type t in _contextParsers.Keys)
            {
                var d = new Dictionary<String, Object>();
                dict.Add(t, d);
            }
            return dict;
        }
    }


    public delegate Object ContentParserDelegate(FileSystemPath path, ContentContext context);
}
