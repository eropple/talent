﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Anaeax.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SharpFileSystem;
using Talent.Graphics.Fonts;
using Talent.Graphics.Fonts.BMFont;
using Talent.Graphics.Fonts.Spec;
using Talent.Graphics.Sources;
using Talent.Scripting;

namespace Talent.Content
{
    public static class BuiltinParsers
    {
        public static Texture2D GetTexture2D(FileSystemPath path, ContentContext context)
        {
            var fs = Files.GameData;
            using (var s = fs.OpenFile(path, FileAccess.Read))
            {
                return Texture2D.FromStream(Runner.DeviceManager.GraphicsDevice, s);
            }
        }

        public static TextureSource GetTextureSource(FileSystemPath path, ContentContext context)
        {
            return new TextureSource(context.Load<Texture2D>(path.ToString()));
        }


        public static AtlasSource GetAtlasSource(FileSystemPath path, ContentContext context)
        {
            using (var s = Files.GameData.OpenFile(path, FileAccess.Read))
            {
                XDocument doc = XDocument.Load(s);
                XElement root = doc.Element("TextureAtlas");

                String imagePath = root.Attribute("imagePath").Value;

                Texture2D texture = context.Load<Texture2D>(path.ParentPath.AppendPath(imagePath).ToString());

                var entries = new List<KeyValuePair<String, Rectangle>>();

                foreach (var sprite in root.Elements("sprite"))
                {
                    String n = sprite.Attribute("n").Value;

                    Int32 x = Int32.Parse(sprite.Attribute("x").Value);
                    Int32 y = Int32.Parse(sprite.Attribute("y").Value);
                    Int32 w = Int32.Parse(sprite.Attribute("w").Value);
                    Int32 h = Int32.Parse(sprite.Attribute("h").Value);

                    entries.Add(new KeyValuePair<String, Rectangle>(n, new Rectangle(x, y, w, h)));
                }

                return new AtlasSource(texture, entries);
            }
        }


        public static TextBuilder GetTextBuilder(FileSystemPath path, ContentContext context)
        {
            FontFile file = null;

            using (var s = Files.GameData.OpenFile(path, FileAccess.Read))
            using (TextReader textReader = new StreamReader(s))
            {
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(FontFile));
                    file = (FontFile)deserializer.Deserialize(textReader);
                }
            }

            FontSpec spec = new FontSpec(file);

            var dict = new Dictionary<Int32, TextureSource>(spec.TexturePageInfo.Count);
            foreach (var kvp in spec.TexturePageInfo)
            {
                dict.Add(kvp.Key, context.Load<TextureSource>(path.ParentPath.AppendPath(kvp.Value).ToString()));
            }

            return new TextBuilder(spec, dict);
        }

        public static AnimationSource GetAnimationSource(FileSystemPath path, ContentContext context)
        {
            XDocument doc = XDocument.Load(Files.GameData.OpenFile(path, FileAccess.Read));
            XElement root = doc.Element("Animation");

            Int32 duration = Int32.Parse(root.Attribute("duration").Value);
            AtlasSource atlas = context.Load<AtlasSource>(root.Attribute("atlasSource").Value);

            IEnumerable<TextureSliceSource> frames = root.Elements("frame").Select(x => atlas[x.Attribute("name").Value]);

            return new AnimationSource(duration, frames);
        }

        public static String GetString(FileSystemPath path, ContentContext context)
        {
            using (var s = Files.GameData.OpenFile(path, FileAccess.Read))
            {
                return s.ReadAllText();
            }
        }

        public static Script GetScript(FileSystemPath path, ContentContext context)
        {
            return new Script(path.ToString(), context.Load<String>(path.ToString()));
        }
    }
}
