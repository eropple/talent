﻿using System;
using Anaeax.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Talent.Content;
using Talent.ControlFlow;
using Talent.Input;

namespace Talent
{
#if MONOGAME
    internal class Runner : Microsoft.Xna.Framework.Game
    {
        public static Runner Instance { get; private set; }
        public static GraphicsDeviceManager DeviceManager { get; private set; }

        public StateSystem State { get; private set; }
        public readonly InputSystem Input;
        public readonly Startup.Options Options;

        private GraphicsDevice _device;
        private Texture2D _whiteTexture;
        private SpriteBatch _batch;

        public Runner(Startup.Options options)
        {
            DeviceManager = new GraphicsDeviceManager(this);
            Input = new InputSystem();
            Options = options;

            Instance = this;
        }

        protected override void Initialize()
        {
            base.Initialize();

#if DESKTOP
            Window.Title = Platform.Product;
#endif
            ContentContext ctx = new ContentContext(DeviceManager, 
                Options.UseDefaultContentParsers, Options.CustomContentParsers);

            State = new StateSystem(DeviceManager, ctx);
            
            State.Push(Options.InitialStateDelegate());

            Input.WatchGamePad(PlayerIndex.One);
            Input.WatchGamePad(PlayerIndex.Two);
            Input.WatchGamePad(PlayerIndex.Three);
            Input.WatchGamePad(PlayerIndex.Four);

            Input.InputHandler = State;

            var x = Mods.ActiveMods;

            _device = DeviceManager.GraphicsDevice;
            _whiteTexture = new Texture2D(_device, 1, 1);
            _whiteTexture.SetData(new[] { Color.White });


            TargetElapsedTime = TimeSpan.FromSeconds(1.0 / Configuration.Get("fps.cap", 60));

            _batch = new SpriteBatch(_device);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            // if we're not active, stop.
            if (!IsActive) return;

            Input.Update();
            State.Update(gameTime.ElapsedGameTime.TotalSeconds);
        }


        protected override bool BeginDraw()
        {
            return base.BeginDraw();
        }
        protected override void Draw(GameTime gameTime)
        {
            _device.ScissorRectangle = _device.PresentationParameters.Bounds;

            _batch.Begin();
            _batch.Draw(_whiteTexture, _device.PresentationParameters.Bounds, null, Color.Black);
            _batch.End();

            // TODO: if we want to do scaling, establish viewport and scissor here

            State.Draw(gameTime.ElapsedGameTime.TotalSeconds);

            base.Draw(gameTime);
        }
        protected override void EndDraw()
        {
            base.EndDraw();
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
        }

        protected override void OnActivated(object sender, EventArgs args)
        {
            base.OnActivated(sender, args);
            Log.Info("OnActivated.");

#if DESKTOP
            Window.Title = Window.Title.Replace(" (PAUSED)", "");
#endif
        }

        protected override void OnDeactivated(object sender, EventArgs args)
        {
            base.OnDeactivated(sender, args);
            Log.Info("OnDectivated.");

#if DESKTOP
            Window.Title = Window.Title + " (PAUSED)";
#endif
        }
    }
#endif
}
