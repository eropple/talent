﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Scripting.SSharp.Runtime;
using Talent.Graphics.Fonts;
using Talent.Graphics.Sources;
using Talent.Graphics.Sprites;

namespace Talent.Scripting
{
    public class Scripts
    {
        public static void Initialize()
        {
            RuntimeHost.Initialize();

            RuntimeHost.AssemblyManager = GetAssemblyManager();
        }

        private static AssemblyManager GetAssemblyManager()
        {
            var a = new AssemblyManager();

            #region Core Types
            a.AddType("String", typeof(String));
            a.AddType("Int32", typeof(Int32));
            a.AddType("Byte", typeof(Byte));
            a.AddType("Int64", typeof(Int64));
            a.AddType("Char", typeof(Char));
            a.AddType("Boolean", typeof(Boolean));
            a.AddType("Double", typeof(Double));
            a.AddType("Single", typeof(Single));
            a.AddType("Object", typeof(Object));

            a.AddType("Array", typeof(List<Object>));
            #endregion

            #region MonoGame Types
            a.AddType("Color", typeof(Color));
            a.AddType("Vector2", typeof(Vector2));
            a.AddType("Vector3", typeof(Vector3));
            a.AddType("Point", typeof(Point));
            a.AddType("Rectangle", typeof(Rectangle));
            #endregion

            #region System
            a.AddType("Random", typeof(Random));
            #endregion

            #region System.Text
            a.AddType("StringBuilder", typeof(StringBuilder));
            #endregion

            #region Game Types
            a.AddType("Log", typeof(Log));

            a.AddType("TextBuilder", typeof(TextBuilder));

            #region Sources
            a.AddType("IDrawSource", typeof(IDrawSource));
            a.AddType("AnimationSource", typeof(AnimationSource));
            a.AddType("TextureSource", typeof(TextureSource));
            a.AddType("TextureSliceSource", typeof(TextureSliceSource));
            a.AddType("AtlasSource", typeof(AtlasSource));
            #endregion
            #region Sprites
            a.AddType("Sprite", typeof(Sprite));
            a.AddType("SimpleSprite", typeof(SimpleSprite));
            #endregion
            #endregion

            return a;
        }
    }
}
