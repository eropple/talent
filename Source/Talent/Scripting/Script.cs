﻿using System;
using Scripting.SSharp.Runtime;

namespace Talent.Scripting
{
    public class Script
    {
        public readonly String Name;

        private readonly Logger _logger;
        private readonly global::Scripting.SSharp.Script _script;

        public ScriptContext Context { get; set; }

        public Script(String name, String scriptContents)
        {
            Name = name;
            _script = global::Scripting.SSharp.Script.Compile(scriptContents);
            _logger = new Logger(this);
            Context = new ScriptContext();
        }

        public Object Execute()
        {
            try
            {
                _script.Context = Context;
                Context.SetItem("Log", _logger);
                Object o = _script.Execute();

#if DEBUG
                if (Log.Level == Log.LoggingLevel.Trace)
                {
                    _logger.Trace("Returned {0} ({1})", o, o.GetType());
                }
#endif
                return o;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }


        public class Logger
        {
            private readonly String _name;

            public Logger(Script script)
            {
                _name = script.Name;
            }

            public void Trace(String text) { Log.Trace(_name + ": " + text); }
            public void Trace(String text, Object o1)
            { Log.Trace(String.Format(_name + ": " + text, o1)); }
            public void Trace(String text, Object o1, Object o2)
            { Log.Trace(String.Format(_name + ": " + text, o1, o2)); }
            public void Trace(String text, Object o1, Object o2, Object o3)
            { Log.Trace(String.Format(_name + ": " + text, o1, o2, o3)); }
            public void Trace(String text, Object o1, Object o2, Object o3, Object o4)
            { Log.Trace(String.Format(_name + ": " + text, o1, o2, o3, o4)); }
            public void Trace(String text, Object o1, Object o2, Object o3, Object o4, Object o5)
            { Log.Trace(String.Format(_name + ": " + text, o1, o2, o3, o4, o5)); }
            public void Trace(String text, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6)
            { Log.Trace(String.Format(_name + ": " + text, o1, o2, o3, o4, o5, o6)); }


            public void Debug(String text) { Log.Debug(_name + ": " + text); }
            public void Debug(String text, Object o1)
            { Log.Debug(String.Format(_name + ": " + text, o1)); }
            public void Debug(String text, Object o1, Object o2)
            { Log.Debug(String.Format(_name + ": " + text, o1, o2)); }
            public void Debug(String text, Object o1, Object o2, Object o3)
            { Log.Debug(String.Format(_name + ": " + text, o1, o2, o3)); }
            public void Debug(String text, Object o1, Object o2, Object o3, Object o4)
            { Log.Debug(String.Format(_name + ": " + text, o1, o2, o3, o4)); }
            public void Debug(String text, Object o1, Object o2, Object o3, Object o4, Object o5)
            { Log.Debug(String.Format(_name + ": " + text, o1, o2, o3, o4, o5)); }
            public void Debug(String text, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6)
            { Log.Debug(String.Format(_name + ": " + text, o1, o2, o3, o4, o5, o6)); }

            public void Info(String text) { Log.Info(_name + ": " + text); }
            public void Info(String text, Object o1)
            { Log.Info(String.Format(_name + ": " + text, o1)); }
            public void Info(String text, Object o1, Object o2)
            { Log.Info(String.Format(_name + ": " + text, o1, o2)); }
            public void Info(String text, Object o1, Object o2, Object o3)
            { Log.Info(String.Format(_name + ": " + text, o1, o2, o3)); }
            public void Info(String text, Object o1, Object o2, Object o3, Object o4)
            { Log.Info(String.Format(_name + ": " + text, o1, o2, o3, o4)); }
            public void Info(String text, Object o1, Object o2, Object o3, Object o4, Object o5)
            { Log.Info(String.Format(_name + ": " + text, o1, o2, o3, o4, o5)); }
            public void Info(String text, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6)
            { Log.Info(String.Format(_name + ": " + text, o1, o2, o3, o4, o5, o6)); }
            
            public void Warn(String text) { Log.Warn(_name + ": " + text); }
            public void Warn(String text, Object o1)
            { Log.Warn(String.Format(_name + ": " + text, o1)); }
            public void Warn(String text, Object o1, Object o2)
            { Log.Warn(String.Format(_name + ": " + text, o1, o2)); }
            public void Warn(String text, Object o1, Object o2, Object o3)
            { Log.Warn(String.Format(_name + ": " + text, o1, o2, o3)); }
            public void Warn(String text, Object o1, Object o2, Object o3, Object o4)
            { Log.Warn(String.Format(_name + ": " + text, o1, o2, o3, o4)); }
            public void Warn(String text, Object o1, Object o2, Object o3, Object o4, Object o5)
            { Log.Warn(String.Format(_name + ": " + text, o1, o2, o3, o4, o5)); }
            public void Warn(String text, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6)
            { Log.Warn(String.Format(_name + ": " + text, o1, o2, o3, o4, o5, o6)); }
            
            public void Error(String text) { Log.Error(_name + ": " + text); }
            public void Error(String text, Object o1)
            { Log.Error(String.Format(_name + ": " + text, o1)); }
            public void Error(String text, Object o1, Object o2)
            { Log.Error(String.Format(_name + ": " + text, o1, o2)); }
            public void Error(String text, Object o1, Object o2, Object o3)
            { Log.Error(String.Format(_name + ": " + text, o1, o2, o3)); }
            public void Error(String text, Object o1, Object o2, Object o3, Object o4)
            { Log.Error(String.Format(_name + ": " + text, o1, o2, o3, o4)); }
            public void Error(String text, Object o1, Object o2, Object o3, Object o4, Object o5)
            { Log.Error(String.Format(_name + ": " + text, o1, o2, o3, o4, o5)); }
            public void Error(String text, Object o1, Object o2, Object o3, Object o4, Object o5, Object o6)
            { Log.Error(String.Format(_name + ": " + text, o1, o2, o3, o4, o5, o6)); }
        }
    }
}
