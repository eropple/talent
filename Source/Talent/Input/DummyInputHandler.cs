﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Talent.Input
{
    public class DummyInputHandler : IInputHandler
    {
        public bool KeyDown(Keys k, bool control, bool shift, bool alt)
        {
            Log.Debug("KeyDown: {0} (ctrl={1}, shift={2}, alt={3}", k, control, shift, alt);
            return true;
        }

        public bool KeyUp(Keys k)
        {
            Log.Debug("KeyUp: {0}", k);
            return true;
        }

        public bool MouseButtonPressed(MouseButton button, Vector2 position, bool control, bool shift, bool alt)
        {
            Log.Debug("MouseButtonPressed: {0} @ {1} (ctrl={2}, shift={3}, alt={4})", button, position, control, shift, alt);
            return true;
        }

        public bool MouseButtonReleased(MouseButton button, Vector2 position)
        {
            Log.Debug("MouseButtonReleased: {0} @ {1}", button, position);
            return true;
        }

        public bool MouseScrollWheel(int scrollDirection, Vector2 position)
        {
            Log.Debug("MouseScrollWheel: {0} @ {1}", scrollDirection, position);
            return true;
        }

        public bool MouseMoved(Vector2 position, Vector2 delta)
        {
            Log.Debug("MouseMoved: {0} d {1}", delta, position);
            return true;
        }

        public bool TouchPressed(int id, Vector2 position)
        {
            Log.Debug("TouchPressed: {0} @ {1}", id, position);
            return true;
        }

        public bool TouchMoved(int id, Vector2 position, Vector2 delta)
        {
            Log.Debug("TouchMoved: {0} @ {1} d {2}", id, position, delta);
            return true;
        }

        public bool TouchReleased(int id, Vector2 position)
        {
            Log.Debug("TouchReleased: {0} @ {1}", id, position);
            return true;
        }

        public bool GamePadThumbstickMoved(PlayerIndex index, Thumbstick stick, Vector2 position, Vector2 delta)
        {
            Log.Debug("GamePadThumbstickMoved: {0}, {1} stick; pos {2}, delta {3}", index, stick, position, delta);
            return true;
        }

        public bool GamePadTriggerMoved(PlayerIndex index, Trigger trigger, float position, float delta)
        {
            Log.Debug("GamePadTriggerMoved: {0}, {1} trigger; pos {2}, delta {3}", index, trigger, position, delta);
            return true;
        }

        public bool GamePadEventDown(PlayerIndex index, GamePadEvent eventType)
        {
            Log.Debug("GamePadEventDown: pad {0}, event {1}", index, eventType);
            return true;
        }

        public bool GamePadEventUp(PlayerIndex index, GamePadEvent eventType)
        {
            Log.Debug("GamePadEventUp: pad {0}, event {1}", index, eventType);
            return true;
        }

        public bool GamePadAttached(PlayerIndex index)
        {
            Log.Debug("GamePadAttached: pad {0}", index);
            return true;
        }

        public bool GamePadDetached(PlayerIndex index)
        {
            Log.Debug("GamePadDetached: pad {0}", index);
            return true;
        }
    }
}
