﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Talent.Utils;

namespace Talent
{
    public static class Configuration
    {
        private static readonly Dictionary<String, String> Items;

        static Configuration()
        {
            Log.Info("Configuration: initializing.");
            Items = BuildConfigDictionary();
        }


        public static String Get(String key, String value)
        {
            String s;
            if (Items.TryGetValue(key.ToUpper(), out s)) return s;
            return value;
        }

        public static Int32 Get(String key, Int32 defaultValue)
        {
            String s;
            if (Items.TryGetValue(key.ToUpper(), out s))
            {
                Int32 i;
                if (Int32.TryParse(s, out i)) return i;
            }
            return defaultValue;
        }

        public static Boolean Get(String key, Boolean defaultValue)
        {
            String s;
            if (Items.TryGetValue(key.ToUpper(), out s))
            {
                return Booleans.ExtendedParse(s);
            }
            return defaultValue;
        }

        private static Dictionary<String, String> BuildConfigDictionary()
        {
#if DESKTOP
            Dictionary<String, String> b = ParseFile(Path.Combine(Platform.ExecutableRoot, "config.properties"));
            Dictionary<String, String> u = ParseFile(Path.Combine(Platform.UserRoot, "config.properties"));
            Dictionary<String, String> c = ParseCommandLine();
            Dictionary<String, String> ret = new Dictionary<String, String>(Math.Max(b.Count, u.Count));

            foreach (KeyValuePair<String, String> kvp in c)
            {
                ret.Add(kvp.Key, kvp.Value);
            }
            foreach (KeyValuePair<String, String> kvp in u)
            {
                if (!ret.ContainsKey(kvp.Key)) ret.Add(kvp.Key, kvp.Value);
            }
            foreach (KeyValuePair<String, String> kvp in b)
            {
                if (!ret.ContainsKey(kvp.Key)) ret.Add(kvp.Key, kvp.Value);
            }

            if (Log.Level == Log.LoggingLevel.Debug)
            {
                foreach (KeyValuePair<String, String> kvp in ret)
                {
                    Log.Debug("{0} = {1}", kvp.Key, kvp.Value);
                }
            }

            return ret;
#else
#error TODO: Implement Configuration() setup for this platform.
#endif
        }



        private static Dictionary<String, String> ParseCommandLine()
        {
#if DESKTOP
            List<String> args = new List<String>(Environment.GetCommandLineArgs());
            args.RemoveAt(0); // the executable name isn't needed

            Log.Debug("Parsing {0} command line arguments.", args.Count);

            return (from arg in args
                    where arg.Contains("=")
                    select arg.Split(new[] { '=' }, 2)).ToDictionary(tokens => tokens[0].Trim().ToUpper(),
                                                                       tokens => tokens[1].Trim());
#else
            return new Dictionary<String, String>();
#endif
        }

        private static Dictionary<String, String> ParseFile(String path)
        {
            if (File.Exists(path))
            {
                Log.Debug("Parsing config '{0}'.", path);
                return ParseLines(File.ReadAllLines(path));
            }
            else
            {
                Log.Debug("Config file not found: {0}", path);
                return new Dictionary<String, String>();
            }
        }

        private static Dictionary<String, String> ParseLines(IEnumerable<String> lines)
        {
            return (from line in lines
                    where line.Length != 0 &&
                          line[0] != '#' &&
                          line[0] != ';' &&
                          line.Contains("=")
                    select line.Split(new[] { '=' }, 2)).ToDictionary(tokens => tokens[0].Trim().ToUpper(),
                                                                        tokens => tokens[1].Trim());
        }
    }
}
