﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Talent.Utils.Collections
{
    public class SingletonList<T> : IReadOnlyList<T>
    {
        public readonly T Value;

        public SingletonList(T value)
        {
            Value = value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            yield return Value;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count
        {
            get { return (Value == null) ? 0 : 1; }
        }

        public T this[int index]
        {
            get
            {
                if (index == 0 && Value != null) return Value;
                throw new IndexOutOfRangeException();
            }
        }
    }
}
