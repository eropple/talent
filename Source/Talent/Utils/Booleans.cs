﻿using System;
using System.Collections.Generic;

namespace Talent.Utils
{
    public static class Booleans
    {
        private static readonly HashSet<String> BooleanLiterals = new HashSet<String>()
        {
            "TRUE",
            "YES",
            "ON",
            "1"
        };

        public static Boolean ExtendedParse(String input)
        {
            String i = input.Trim().ToUpper();
            return BooleanLiterals.Contains(i);
        }
    }
}
