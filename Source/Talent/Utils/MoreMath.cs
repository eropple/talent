﻿using System;

namespace Talent.Utils
{
    public static class MoreMath
    {

        public static Double Clamp(Double value, Double min, Double max)
        {
            return value < min ? min : value > max ? max : value;
        }
    }
}
