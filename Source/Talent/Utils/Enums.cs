﻿using System;

namespace Talent.Utils
{
    public static class Enums
    {
        public static T Parse<T>(String text, Boolean ignoreCase = true) where T : struct
        {
            return (T)Enum.Parse(typeof(T), text, ignoreCase);
        }
    }
}
