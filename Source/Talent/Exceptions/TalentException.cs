﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Talent.Exceptions
{
    public class TalentException : Exception
    {
        public TalentException()
        {
        }

        public TalentException(string message) : base(message)
        {
        }

        public TalentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
